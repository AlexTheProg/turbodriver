package com.turbodriver.notification.interfaces.email;

import com.turbodriver.notification.model.EmailDetails;

public interface EmailService {
    String sendSimpleEmail(EmailDetails emailDetails);
}
