package com.turbodriver.notification.service;

import com.turbodriver.clients.notification.NotificationRequest;
import com.turbodriver.notification.model.EmailDetails;
import com.turbodriver.notification.model.Notification;
import com.turbodriver.notification.repository.NotificationRepository;
import lombok.AllArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final EmailServiceImpl emailService;

    public void send(NotificationRequest notificationRequest) {
        notificationRepository.save(
                Notification.builder()
                        .toCustomerId(notificationRequest.customerId())
                        .toCustomerEmail(notificationRequest.customerEmail())
                        .sender("Turbodriver")
                        .message(notificationRequest.message())
                        .sentAt(LocalDateTime.now())
                        .build()
        );
        sendEmail(notificationRequest);
    }

    private void sendEmail(NotificationRequest notificationRequest){
        EmailDetails emailDetails = createMailDetails(notificationRequest);
        emailService.sendSimpleEmail(emailDetails);
    }

    private EmailDetails createMailDetails(NotificationRequest request){
        EmailDetails details = new EmailDetails();

        details.setRecipient(request.customerEmail());
        details.setBody(request.message());

        return details;
    }

}
