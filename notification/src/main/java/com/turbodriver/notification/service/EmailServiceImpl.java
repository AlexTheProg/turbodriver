package com.turbodriver.notification.service;

import com.turbodriver.notification.interfaces.email.EmailService;
import com.turbodriver.notification.model.EmailDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public String sendSimpleEmail(EmailDetails emailDetails) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sender);
        message.setTo(emailDetails.getRecipient());
        message.setSubject("Welcome to Turbodriver");
        message.setText(emailDetails.getBody());

        javaMailSender.send(message);
        return null;
    }

}
