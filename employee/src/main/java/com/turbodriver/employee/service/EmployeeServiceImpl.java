package com.turbodriver.employee.service;

import com.turbodriver.amqp.RabbitMQMessageProducer;
import com.turbodriver.clients.carfleet.CarFleetClient;
import com.turbodriver.clients.carfleet.CarGetDto;
import com.turbodriver.clients.notification.NotificationClient;
import com.turbodriver.clients.notification.NotificationRequest;
import com.turbodriver.employee.exception.UserAlreadyExists;
import com.turbodriver.employee.exception.UserNotFound;
import com.turbodriver.employee.exposition.dto.EmployeeRegistrationRequest;
import com.turbodriver.employee.model.Employee;
import com.turbodriver.employee.model.Job;
import com.turbodriver.employee.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepo;
    private final CarFleetClient carFleetClient;
    private final RabbitMQMessageProducer messageProducer;

    @Override
    public void registerEmployee(EmployeeRegistrationRequest request) {
        Employee employee = Employee.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .dateOfBirth(Date.from(Instant.now()))
                .hiringDate(Date.from(Instant.now()))
                .jobTitle(Job.valueOf(request.getJobTitle()))
                .salary(BigDecimal.valueOf(1000))
                .build();
        if(employeeRepo.findByEmail(request.getEmail()).isPresent()){
            throw new UserAlreadyExists();
        }
        employeeRepo.saveAndFlush(employee);

        NotificationRequest notificationRequest = new NotificationRequest(
                employee.getId(),
                employee.getEmail(),
                "Welcome to Turbodriver. Enjoy your stay!"
        );

        messageProducer.publish(
                notificationRequest,
                "internal.exchange",
                "internal.notification.routing-key"
        );
    }



    @Override
    public Employee getEmployee(String id) {
        return employeeRepo.findById(Integer.parseInt(id)).orElseThrow(UserNotFound::new);
    }

    @Override
    public CarGetDto checkDriverCarData(String id) {
        Employee employee = employeeRepo
                .findById(Integer.parseInt(id))
                .orElseThrow(UserNotFound::new);
        return carFleetClient.getCarByDriver(id);
    }

}
